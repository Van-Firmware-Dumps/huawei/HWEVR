## generic_a15-user 10 QP1A.190711.020 eng.root.20240131.125256 dev-keys
- Manufacturer: unknown
- Platform: kirin980
- Codename: kirin980
- Brand: kirin980
- Flavor: user
- Release Version: 10
- Kernel Version: 
- Id: QP1A.190711.020
- Incremental: eng.root.20240124.003754
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: kirin980/kirin980/kirin980:10/QP1A.190711.020/root202401240037:user/test-keys
- OTA version: 
- Branch: generic_a15-user-10-QP1A.190711.020-eng.root.20240131.125256-dev-keys
- Repo: kirin980/kirin980
